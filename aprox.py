import sys

def compute_trees(trees):

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production

def compute_all(min_trees, max_trees):
    productions = []
    for i in range(min_trees,max_trees+1):
        produccion = compute_trees(i)
        productions = productions + [(i,produccion)]
    return productions

def read_arguments():

    try:
        base_trees = int(sys.argv[1])
        fruit_per_tree = int(sys.argv[2])
        reduction = int(sys.argv[3])
        min = int(sys.argv[4])
        max = int(sys.argv[5])
        return base_trees, fruit_per_tree, reduction, min, max
    except:
        raise ValueError("All arguments must be integers")

def main():
    global base_trees
    global fruit_per_tree
    global reduction
    base_trees, fruit_per_tree, reduction, min, max = read_arguments()
    productions = compute_all(min, max)
    for i in range(0,len(productions)):
        if i%2 != 0:
            productions[i-2] > productions[i]
            menor = productions[i]
            best_production = productions[i-2]
        elif i%2 == 0:
            productions[i - 2] > productions[i]
            menor = productions[i]
            best_trees = productions[i-2]
    print(f"Best production: {best_production}, for {best_trees} trees")

if __name__ == '__main__':
    main()
